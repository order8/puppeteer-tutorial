const puppeteer = require('puppeteer');
const chalk = require('chalk');
const fs = require('fs');

const error = chalk.bold.red;
const success = chalk.keyword('green');

(async() => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();

  const urls = [
    'https://www.specialtycoffee.jp/beans/2401.html',
    'https://www.specialtycoffee.jp/beans/19.html',
    'https://www.specialtycoffee.jp/beans/2397.html'
  ];

  const getBeanInfo = async(url) => {
    await page.goto(url);
    await page.waitForSelector('div#spec h4.caption');

    const beansInfoScraped = await page.evaluate(() => {
      const beanInfo = {};
      beanInfo['pageTitle'] = document.querySelector('.namejap').innerText;

      const trs = document.querySelectorAll('div#spec tr');
      trs.forEach((tr) => {
        const tds = tr.querySelectorAll('td');
        let keyValuePair = [];
        tds.forEach((td) => {
          keyValuePair.push(td.innerText);
        });
        const key = keyValuePair[0];
        const value = keyValuePair[1];
        beanInfo[key] = value;
      });
      return beanInfo;
    });

    return beansInfoScraped;
  }

  const main = async() => {
    const beanRecords = await Promise.all(urls.map(async(url) => {
      const beanRecord = await getBeanInfo(url);
      console.log(url);
      return beanRecord;
    }));

    await browser.close();
    saveJson('beans_from_ataka.json', beanRecords);
  }

  const saveJson = (name, date) => {
    fs.writeFile(name, JSON.stringify(date), () => {
      console.log(success('Saved!'));
    });
  }

  main();

})();