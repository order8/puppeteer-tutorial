const puppeteer = require('puppeteer');
const chalk = require('chalk');

// colorful console.logs
const error = chalk.bold.red;
const success = chalk.keyword('green');

(async() => {
  // open the headless browser in non-headless mode
  const browser = await puppeteer.launch({ headless: false });
  // open a new page
  const page = await browser.newPage();

  try {

    // generate unique filename
    const filename = 'save_files/' + Date.now() + '-example.png';
    // enter url in page
    await page.goto('http://www.google.com/');
    // Google Say Cheese!!
    await page.screenshot({ path: filename });
    await closeBrowser(browser);

  } catch (err) {

    // Catch and display errors
    console.log(error(err));
    await closeBrowser(browser, err);

  }
})();

// close browser
const closeBrowser = async(browser, err) => {
  browser.close();
  if (err) {
    console.log(error('Browser Closed'));
  } else {
    console.log(success('Browser Closed'));
  }
}