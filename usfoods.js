const puppeteer = require('puppeteer');
const chalk = require('chalk');
const fs = require('fs');

const error = chalk.bold.red;
const success = chalk.keyword('green');

(async() => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();

  const urls = [
    'https://usfoods.co.jp/shopping/view.php?id=926',
    'https://usfoods.co.jp/shopping/view.php?id=599'
  ];

  const getBeanInfo = async(url) => {
    await page.goto(url);
    await page.waitForSelector('h1.hdl03');

    const beansInfoScraped = await page.evaluate(() => {
      const beanInfo = {};
      beanInfo['pageTitle'] = document.querySelector('h1.hdl03').innerText;

      const beanDetails = document.querySelectorAll('.item-detail');

      if (beanDetails.length == 3) {
        let count = 0;
        beanDetails.forEach((detail) => {
          if (count < 2) {
            const detailChildren = detail.children;
            for (let i = 0; i < detailChildren.length; i++) {
              if (detailChildren[i].tagName == 'DT') {
                let dt = detailChildren[i].innerText;
                const dd = detailChildren[i].nextElementSibling.innerText;
                dt = dt.substring(0, dt.length - 1);
                beanInfo[dt] = dd;
              }

            }
          }
          count++;
        });
      } else {
        console.log('異なる豆情報の表示フォーマットを検知したので処理をスキップ: item-detail length = ' + beanDetails.length);
      }

      return beanInfo;
    });

    return beansInfoScraped;
  }

  const saveJson = async(name, date) => {
    fs.writeFile(name, JSON.stringify(date), () => {
      console.log(success('Saved!'));
    });
  }

  const main = async() => {
    const beanRecords = await Promise.all(urls.map(async(url) => {
      const beanRecord = await getBeanInfo(url);
      return beanRecord;
    }));

    await browser.close();
    await saveJson('beans_from_usfoods.json', beanRecords);
  }

  main();

})();